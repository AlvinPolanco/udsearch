import requests
import discord
import json
import os
import random
from discord.ext import commands

client = commands.Bot(command_prefix='/', cas_insensitive=True)
TOKEN = os.getenv('UD_DISCORD_TOKEN')
APIKEY = os.getenv('API_KEY')

url = "https://mashape-community-urban-dictionary.p.rapidapi.com/define"

headers = {
	"x-rapidapi-host": "mashape-community-urban-dictionary.p.rapidapi.com",
	"x-rapidapi-key": APIKEY
}

def getlist(udquery):
	querystring = {"term": udquery}
	response = requests.request("GET", url, headers=headers, params=querystring)
	definitions = response.json()
	return definitions['list']

@client.event
async def on_ready():
	await client.change_presence(status=discord.Status.online, activity=discord.Activity(type=discord.ActivityType.listening, name="/udhelp"))
	print("Bot is ready.")

@client.event
async def on_command_error(ctx, error):
	if isinstance(error, commands.CommandNotFound):
		await ctx.send("Invalid command. Please use /udhelp for commands.")

@client.command(aliases=['dictme', 'UDL', 'udl'])
async def udlookup(ctx, *, udquery):
	definitions = getlist(udquery)
	if definitions == []:
		embed = discord.Embed(description=f"Sorry, there is no entry for {udquery}.", color=0x2f48e3)
	else:
		udpermalink = definitions[0]['permalink']
		embed = discord.Embed(title=udquery, url=udpermalink, color=0x2f48e3)
		count = 1
		for item in definitions:
			embed.add_field(name=f"{count}.", value=item['definition'], inline=False)
			count += 1
	await ctx.send(embed=embed)

@client.command(aliases=['UDT', 'udt'])
async def udtop(ctx, *, udquery):
	definitions = getlist(udquery)
	if definitions == []:
		embed = discord.Embed(description=f"Sorry, there is no entry for {udquery}.", color=0x2f48e3)
	else:
		udpermalink = definitions[0]['permalink']
		embed = discord.Embed(title=udquery, url=udpermalink, color=0x2f48e3)
		embed.add_field(name="Top Definition", value=definitions[0]['definition'], inline=False)
		embed.set_footer(text=f"👍 {definitions[0]['thumbs_up']}\t\t👎 {definitions[0]['thumbs_down']}")
	await ctx.send(embed=embed)

@client.command(aliases=['UDR', 'udr'])
async def udrandom(ctx, *, udquery):
	definitions = getlist(udquery)
	if definitions == []:
		embed = discord.Embed(description=f"Sorry, there is no entry for {udquery}.", color=0x2f48e3)
	else:
		udpermalink = definitions[0]['permalink']
		randef=random.randint(1,(len(definitions)+1))
		embed = discord.Embed(title=udquery, url=udpermalink, color=0x2f48e3)
		embed.add_field(name="Random Definition", value=definitions[randef]['definition'], inline=False)
		embed.set_footer(text=f"👍 {definitions[randef]['thumbs_up']}\t\t👎 {definitions[randef]['thumbs_down']}")
	await ctx.send(embed=embed)

@client.command(aliases=['UDH', 'udh'])
async def udhelp(ctx):
	embed = discord.Embed(title="UDSearch Help Menu", description="Urban Dictionary search tool commands.", color=0x2f48e3)
	embed.add_field(name="/udlookup <searchitem>", value="Returns the full list of definitions for <searchitem>.\nAliases:/UDL", inline=False)
	embed.add_field(name="/udtop <searchitem>", value="Returns definition with highest net thumbs up to down count.\nAliases:/UDT", inline=False)
	embed.add_field(name="/udrandom <searchitem>", value="Returns a random definition for <searchitem>.\nAliases:/UDR", inline=False)
	await ctx.send(embed=embed)

client.run(TOKEN)
